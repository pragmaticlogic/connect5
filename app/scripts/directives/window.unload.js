//Modeled after https://groups.google.com/forum/#!topic/angular/-d7Sf6Qqm_I
angular.module('window.unload.directives', []).directive('confirmOnExit', [
    function() {
        return {
            link: function(scope, elem, attrs) {
                scope.$on('$locationChangeStart', function(event, next, current) {
                    if(!confirm("Do you want really to quit the game?")) {
                        event.preventDefault();
                    }
                });
                window.onbeforeunload = function() {
                    return "The game is in progress.";
                }
            }
        };
    }
]);