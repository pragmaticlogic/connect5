/**
 * http://stackoverflow.com/users/1244013/khnle
 * http://pragmaticlogic.github.io
 */
angular.module('clickable.grid', []).constant('clickableGridConfig', {
    rows: 20,
    columns: 20
}).directive('clickableGrid', ['$parse', '$timeout', 'clickableGridConfig',
    function(parse, timeout, clickableGridConfig) {
        var lastClicked = 'O',
            //stackoverflow.com/questions/4257545/how-to-check-the-class-in-dom
            hasClass = (typeof document.documentElement.classList == "undefined") ? function(el, clss) {
                return el.className && new RegExp("(^|\\s)" + clss + "(\\s|$)").test(el.className);
            } : function(el, clss) {
                return el.classList.contains(clss);
            };
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            //scope: {
            //	callback: '&onCellClick',
            //	gameData: '=gameState'
            //},
            link: function(scope, elem, attrs) {
                var i = 0,
                    grid = document.createElement('table');
                grid.className = 'grid';
                for(var r = 0; r < clickableGridConfig.rows; ++r) {
                    var tr = grid.appendChild(document.createElement('tr'));
                    for(var c = 0; c < clickableGridConfig.columns; ++c) {
                        var cell = tr.appendChild(document.createElement('td'));
                        cell.id = r + '-' + c;
                        cell.addEventListener('click', (function(el, r, c, i) {
                            return function() {
                                if(hasClass(el, 'played') === false && scope.gameTurn === true && scope.gameEnded === false) {
                                    el.innerHTML = scope.gameChar;
                                    el.className = scope.gameChar.toLowerCase();
                                    var tId = timeout(function() {
                                        el.className = el.className + ' played';
                                        timeout.cancel(tId);
                                    }, 2000);
                                    scope.gameTurn = false;
                                    scope.$apply(function() {
                                        scope.gameMoveToOpponent = {
                                            row: r,
                                            column: c
                                        };
                                    });
                                    //scope.callback({r: r, c: c});
                                }
                            };
                        })(cell, r, c, i), false);
                    }
                }
                elem.replaceWith(grid);
                //or scope.$watch(attrs.gameMoveInbound, function(oldVal, newVal) {
                scope.$watch('gameMoveFromOpponent', function(newVal) {
                    var row = newVal.row;
                    var column = newVal.column;
                    var el = document.getElementById(row + '-' + column);
                    if(el !== null) {
                        el.innerHTML = (scope.gameChar === 'X' ? 'O' : 'X');
                        el.className = (scope.gameChar === 'X' ? 'o played' : 'x played');
                    }
                });
                scope.$watch('winningCells', function(cells) {
                    _.forEach(cells, function(cell) {
                        var row = cell.row;
                        var column = cell.column;
                        var el = document.getElementById(row + '-' + column);
                        if(el !== null) {
                            el.className = el.className + ' win';
                        }
                    });
                });
            }
        }
    }
]);