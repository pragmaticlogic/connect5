(function(module) {
    'use strict';
    module.controller('MainCtrl', ['$scope', '$http', '$rootScope', '$q', '$modal', '$timeout', '$location', 'socket', 'timerService', 'appDataService',
        function($scope, $http, $rootScope, $q, $modal, $timeout, $location, socket, timerService, appDataService) {
            var MAX = 60,
                modalInstance = undefined,
                modalCallback = undefined,
                timeDiff = 0;
            //All private stuffs
            //------------------------------------------------
            var addAlertInvite = function(invitorNickname, invitorSocketId) {
                var alert = {
                    type: 'success',
                    msg: 'You receive invitation from ' + invitorNickname,
                    invitorNickname: invitorNickname,
                    clientId: invitorSocketId,
                    startTime: Date.now(),
                    dynamic: MAX
                },
                    callback = function(alert, cb) {
                        var now = Date.now();
                        alert.dynamic = MAX - Math.floor((now - alert.startTime) / 1000);
                        if(alert.dynamic <= 0) {
                            timerService.unsubscribe(cb);
                            var index = findAlertInvite(alert);
                            //Time expires, do NOT want to post reject invite back
                            $scope.closeAlertInvite(index, false);
                        }
                    },
                    size = $scope.alertsInvite.push(alert) - 1;
                alert.callback = callback;
                timerService.subscribe(callback, 1, [alert]);
                return size;
            };
            var findAlertInvite = function(alert) {
                var i;
                for(i = 0; i < $scope.alertsInvite.length; i++) {
                    if($scope.alertsInvite[i] === alert) return i;
                }
            };
            var addAlertException = function(msg) {
                var WAIT_TIME = 4,
                    alert = {
                        type: 'danger',
                        msg: msg,
                        startTime: Date.now()
                    },
                    callback = function(alert, cb) {
                        var dynamic = WAIT_TIME - Math.floor((Date.now() - alert.startTime) / 1000);
                        if(dynamic <= 0) {
                            timerService.unsubscribe(cb);
                            var index = findAlertException(alert);
                            $scope.closeAlertException(index);
                        }
                    },
                    size = $scope.alertsException.push(alert) - 1;
                alert.callback = callback;
                timerService.subscribe(callback, 1, [alert]);
                return size;
            };
            var findAlertException = function(alert) {
                var i;
                for(i = 0; i < $scope.alertsException.length; i++) {
                    if($scope.alertsException[i] === alert) return i;
                }
            };
            var createRandomAlphaNumericText = function(len) {
                var text = '';
                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                for(var i = 0; i < len; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            };
            var retrieveNickname = function() {
                if(appDataService.getUserSelfNickname() === undefined) {
                    return createRandomAlphaNumericText(8);
                } else {
                    return appDataService.getUserSelfNickname();
                }
            }
            var postCancelInvite = function() {
                if($scope.userInvitee.socketId !== undefined) {
                    $http.post('/cancel-invite', {
                        'inviteFrom': $scope.userSelf.socketId,
                        'inviteTo': $scope.userInvitee.socketId,
                        'fromNickname': $scope.userSelf.nickname
                    });
                    $scope.userInvitee = {
                        nickname: undefined,
                        socketId: undefined
                    };
                }
            };
            socket.on('connect', function() {
                socket.emit('new player', {
                    'nickname': $scope.userSelf.nickname
                });
            });
            socket.on('disconnect', function() {
                //This event is useless, uses remove player instead
            });
            socket.on('new player', function(data) {
                //console.log('new player ' + data.id + ', ' + data.nickname);
            });
            socket.on('remove player', function(data) {
                //console.log('remove player ' + data.id + ', ' + data.nickname);
                //if removed player is someone inviting us, we have to remove the alert
                var i;
                for(i = 0; i < $scope.alertsInvite.length; i++) {
                    if($scope.alertsInvite[i].clientId === data.id) {
                        $scope.closeAlertInvite(i, false); //receive cancel invite, do NOT want to post reject invite back
                    }
                }
                //if removed player is being invited by us, we have to close the invite modal
                if(modalInstance !== undefined) {
                    timerService.unsubscribe(modalCallback);
                    modalInstance.close();
                    $scope.userInvitee = {
                        nickname: undefined,
                        socketId: undefined
                    };
                    modalInstance = undefined;
                }
            });
            //I just connect.  I need to know what my socketId is
            //or this page is the result of the back button, and I need to re-sync my data
            socket.on('my socketId', function(data) {
                $scope.userSelf.socketId = data.socketId;
                appDataService.setUserSelfSocketId(data.socketId);
                if(data.nickname !== undefined) {
                    $scope.userSelf.nickname = data.nickname;
                    appDataService.setUserSelfNickname(data.nickname);
                }
                timeDiff = data.now - Date.now() + 5000; //Assume it takes 5 seconds to get the response from the server
            });
            socket.on('all players', function(data) {
                var i;
                for(i = 0; i < data.players.length; i++) {
                    data.players[i].onlineSince = moment(Math.max(data.players[i].logonTime - timeDiff, 1000)).fromNow();
                    data.players[i].self = (data.players[i].id == $scope.userSelf.socketId);
                }
                //$scope.awesomeThings = angular.toJson(data.players);
                $rootScope.$apply(function() {
                    $scope.players = data.players
                });
            });
            socket.on('invite to play', function(data) {
                $rootScope.$apply(function() {
                    var index = addAlertInvite(data.fromNickname, data.inviteFromClientId);
                });
            });
            socket.on('cancel invite', function(data) {
                //Receive cancel invite, need to close alert
                var invitorNickname, i;
                for(i = 0; i < $scope.alertsInvite.length; i++) {
                    if($scope.alertsInvite[i].clientId === data.inviteFromClientId) {
                        invitorNickname = $scope.alertsInvite[i].invitorNickname;
                        $scope.closeAlertInvite(i, false); //receive cancel invite, do NOT want to post reject invite back
                    }
                }
                addAlertException('This invitation from ' + invitorNickname + ' has been withdrawn.');
            });
            socket.on('reject invite', function(data) {
                var inviteeNickname = $scope.userInvitee.nickname;
                //alternatively data.rejectToClientId
                if(modalInstance !== undefined) {
                    timerService.unsubscribe(modalCallback);
                    modalInstance.close();
                    $scope.userInvitee = {
                        nickname: undefined,
                        socketId: undefined
                    };
                    modalInstance = undefined;
                }
                addAlertException('Your invitation to ' + inviteeNickname + ' was not accepted.');
            });
            socket.on('accept invite', function(data) {
                var opponent = $scope.userInvitee,
                    roomId = data.roomId;
                //alternatively data.acceptFromClientId
                if(modalInstance !== undefined) {
                    timerService.unsubscribe(modalCallback);
                    modalInstance.close();
                    $scope.userInvitee = {
                        nickname: undefined,
                        socketId: undefined
                    };
                    modalInstance = undefined;
                }
                appDataService.setUserOpponent(opponent);
                appDataService.setUserSelfNickname($scope.userSelf.nickname);
                appDataService.setUserSelfSocketId($scope.userSelf.socketId);
                appDataService.goFirst = false;
                //h_tps://gist.github.com/crabasa/2896891
                //h_tp://stackoverflow.com/questions/19150220/creating-rooms-in-socket-io
                //socket.emit('createRoom', roomId);
                appDataService.roomId = roomId;
                $location.path('play');
            });
            //All stuffs below are accessible from views
            //-------------------------------------------
            $scope.players = [];
            $scope.alertsInvite = [];
            $scope.alertsException = [];
            $scope.timerIds = {};
            //Note, make change here.  
            $scope.userSelf = {
                nickname: retrieveNickname(),
                socketId: undefined
            };
            $scope.userInvitee = {
                nickname: undefined,
                socketId: undefined
            };
            //user clicks on the x button of the alert, postHttp must be set to true
            $scope.closeAlertInvite = function(index, postHttp) {
                var alert = $scope.alertsInvite[index];
                var cb = alert.callback;
                timerService.unsubscribe(cb);
                if(postHttp) {
                    $http.post('/reject-invite', {
                        'rejectTo': alert.clientId
                    });
                }
                $scope.alertsInvite.splice(index, 1);
            };
            $scope.closeAlertException = function(index) {
                var alert = $scope.alertsException[index];
                var cb = alert.callback;
                timerService.unsubscribe(cb);
                $scope.alertsException.splice(index, 1);
            };
            $scope.checkName = function(data) {
                if(data.length < 8) {
                    return 'Your nickname must have at least 8 characters';
                }
                var d = $q.defer();
                $http.post('/check-nickname-unique', {
                    'nickname': data
                }).success(function(res) {
                    res = res || {};
                    //if(res.status === 'ok') { // {status: "ok"}
                    if(res.response === 'ok') {
                        d.resolve()
                    } else { // {status: "error", msg: "Username should be `awesome`!"}
                        res.msg = 'Nickname must be unique';
                        d.resolve(res.msg)
                    }
                }).error(function(e) {
                    d.reject('Internal Server Error!');
                });
                return d.promise;
            };
            $scope.updateNickname = function(data) {
                $http.post('/change-nickname', {
                    'clientId': $scope.userSelf.socketId,
                    'nickname': data
                }).success(function() {
                    appDataService.setUserSelfNickname(data);
                });
            };
            $scope.acceptInviteToPlay = function(index) {
                //Note: Once we accept an invite, go through the remaining alerts invite and reject them all
                var invitorNickname = $scope.alertsInvite[index].invitorNickname,
                    invitorSocketId = $scope.alertsInvite[index].clientId;
                var i;
                for(i = 0; i < $scope.alertsInvite.length; i++) {
                    if(i != index) {
                        $scope.closeAlertInvite(i, true); //close alert, and post reject invite back
                    } else {
                        $scope.closeAlertInvite(index, false);
                    }
                }
                var roomId = createRandomAlphaNumericText(10);
                //Post accept invite message to sender
                $http.post('/accept-invite', {
                    'acceptFrom': invitorSocketId,
                    'roomId': roomId
                }).success(function() {
                    appDataService.setUserOpponent({
                        socketId: invitorSocketId,
                        nickname: invitorNickname
                    });
                    appDataService.setUserSelfNickname($scope.userSelf.nickname);
                    appDataService.setUserSelfSocketId($scope.userSelf.socketId);
                    //h_tps://gist.github.com/crabasa/2896891
                    //h_tp://stackoverflow.com/questions/19150220/creating-rooms-in-socket-io
                    //socket.emit('createRoom', roomId);
                    appDataService.roomId = roomId;
                    appDataService.goFirst = true;
                    //Then take to new screen
                    $location.path('play');
                });
            };
            $scope.inviteToPlay = function(playerId, playerNickname) {
                //Note: Once we invite, go through the alerts invite and reject them all
                var i;
                for(i = 0; i < $scope.alertsInvite.length; i++) {
                    $scope.closeAlertInvite(i, true); //close alert, and post reject invite back
                }
                $scope.userInvitee = {
                    nickname: playerNickname,
                    socketId: playerId
                };
                modalInstance = $modal.open({
                    windowClass: 'invite-window-modal',
                    templateUrl: 'modal-receive-invitation.html',
                    backdrop: 'static',
                    controller: ['$scope', '$modalInstance',
                        function($scope, $modalInstance) { //SO 18733680
                            $scope.progressCircleData = {
                                startTime: Date.now(),
                                value: 0
                            };
                            $scope.userInvitee = {
                                nickname: playerNickname,
                                socketId: playerId
                            };
                            modalCallback = function(data, cb) {
                                var now = Date.now();
                                var val = Math.floor((now - $scope.progressCircleData.startTime) / 1000);
                                var diff = MAX - val;
                                $scope.progressCircleData.value = val;
                                if(MAX - val <= 0) {
                                    timerService.unsubscribe(cb);
                                    $modalInstance.close();
                                }
                            };
                            timerService.subscribe(modalCallback, 1, [$scope.progressCircleData]);
                            $scope.ok = function() {
                                timerService.unsubscribe(modalCallback);
                                $modalInstance.close();
                            };
                            $scope.cancel = function() {
                                timerService.unsubscribe(modalCallback);
                                $modalInstance.dismiss('cancel');
                                postCancelInvite();
                            };
                        }
                    ]
                });
                $http.post('/invite-to-play', {
                    'inviteFrom': $scope.userSelf.socketId,
                    'inviteTo': $scope.userInvitee.socketId,
                    'fromNickname': $scope.userSelf.nickname
                });
            };
            //Register this callback with timer service, so that it could update how long the users have been on the site
            (function() {
                timerService.subscribe(function() {
                    var i;
                    for(i = 0; i < $scope.players.length; i++) {
                        $scope.players[i].onlineSince = moment(Math.max($scope.players[i].logonTime - timeDiff, 1000)).fromNow();
                    }
                    $rootScope.$apply(function() {
                        $scope.players = $scope.players
                    });
                }, 40, []);
            })();
        }
    ]);
})(angular.module('caro'));