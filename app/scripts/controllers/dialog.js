(function(module) {
    'use strict';
    module.controller('DialogCtrl', ['$scope', '$modal',
        function($scope, $modal) {
            var modalInstance;
            $scope.showHowToPlay = function() {
                modalInstance = $modal.open({
                    windowClass: 'how-to-play-window-modal',
                    templateUrl: 'modal-how-to-play.html',
                    backdrop: 'static',
                    controller: ['$scope', '$modalInstance',
                        function($scope, $modalInstance) { //SO 18733680
                            $scope.ok = function() {
                                $modalInstance.close();
                            };
                        }
                    ]
                });
            };
            $scope.showAbout = function() {
                modalInstance = $modal.open({
                    windowClass: 'about-window-modal',
                    templateUrl: 'modal-about.html',
                    backdrop: 'static',
                    controller: ['$scope', '$modalInstance',
                        function($scope, $modalInstance) { //SO 18733680
                            $scope.ok = function() {
                                $modalInstance.close();
                            };
                        }
                    ]
                });
            };
        }
    ]);
})(angular.module('caro'));