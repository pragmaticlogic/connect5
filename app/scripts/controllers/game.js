(function(module) {
    'use strict';
    module.controller('GameCtrl', ['$scope', '$http', '$rootScope', '$q', '$modal', '$timeout', '$location', '$window', 'socket', 'timerService', 'appDataService',
        function($scope, $http, $rootScope, $q, $modal, $timeout, $location, $window, socket, timerService, appDataService) {
            $scope.$on('$locationChangeStart', function(event, next, current) {
                // if(!$window.confirm("You are in the middle of a game.  Do you want really to quit?")) {
                // event.preventDefault();
                // 	}
            });
            $scope.$on('$locationChangeSuccess', function(event, next, current) {
                $http.post('/sync-my-data', {
                    myClientId: appDataService.getUserSelfSocketId()
                }).success(function() {
                    $http.post('/broadcast-all-players', {});
                });
                //Tell the server I'm leaving the game.  The server will repeat that to the opponent so.
                socket.emit('leave game', {
                    'opponentSocketId': $scope.userOpponent.socketId
                });
            });
            var SIZE = 20,
                WAIT_TIME_SELF = 10,
                WAIT_TIME_OPPONENT = 12;
            var checkForWin = function(a) {
                var winNums = [];
                var next = [{
                        row: function(r) {
                            return r + 1;
                        },
                        column: function(c) {
                            return c;
                        }
                    }, // next vertical
                    {
                        row: function(r) {
                            return r;
                        },
                        column: function(c) {
                            return c + 1;
                        }
                    }, // next horizontal
                    {
                        row: function(r) {
                            return r + 1;
                        },
                        column: function(c) {
                            return c + 1;
                        }
                    }, // next NW to SE
                    {
                        row: function(r) {
                            return r + 1;
                        },
                        column: function(c) {
                            return c - 1;
                        }
                    } // next NE to SW
                ];
                var check = function(a, row, column, f, match, winNums) {
                    if(match >= 5) {
                        return true;
                    } else if(row < 0 || column < 0 || row >= SIZE || column >= SIZE) {
                        return false;
                    } else {
                        if(a[row][column] == 1) {
                            var rt = check(a, f.row(row), f.column(column), f, match + 1, winNums);
                            if(rt === true) {
                                winNums.push({
                                    row: row,
                                    column: column
                                });
                            }
                            return rt;
                        } else {
                            return false;
                        }
                    }
                };
                var result = false; //let it hoist
                _.forEach(next, function(f) {
                    var match = 0;
                    _.forEach(_.range(SIZE * SIZE), function(num) {
                        if(check(a, Math.floor(num / SIZE), num % SIZE, f, match, winNums) === true) {
                            result = true;
                            return;
                        }
                    });
                });
                if(result === true) {
                    $scope.winningCells = winNums;
                }
                return result;
            };
            var addAlert = function(destination, alert) {
                //unsubscribe all with timer service
                _.forEach(_.pluck($scope.alertsTimer, 'callback'), function(cb) {
                    timerService.unsubscribe(cb);
                });
                _.remove($scope.alertsException);
                _.remove($scope.alertsTimer);
                destination.push(alert);
            };
            var addAlertWithTimer = function(alert, waitTime, waitForSelf) {
                _.assign(alert, {
                    startTime: Date.now(),
                    callback: function(alert, cb) {
                        var dynamic = waitTime - Math.floor((Date.now() - alert.startTime) / 1000);
                        if(dynamic <= 0) {
                            $scope.gameEnded = true;
                            timerService.unsubscribe(cb);
                            _.remove($scope.alertsTimer, alert);
                            addAlert($scope.alertsException, waitForSelf === true ? {
                                msg: 'Time expired on you. You lose. Close this box to go back to the lobby.'
                            } : {
                                msg: 'Time expired on your opponent. You win. Close this box to go back to the lobby.'
                            });
                        }
                    }
                });
                addAlert($scope.alertsTimer, alert);
                timerService.subscribe(alert.callback, 1, [alert]);
            };
            //The server tells me that my opponent disconnects their browser
            socket.on('remove player', function(data) {
                if(data.id == $scope.userOpponent.socketId) {
                    $scope.gameEnded = true;
                    addAlert($scope.alertsException, {
                        msg: 'Your opponent ' + $scope.userOpponent.nickname + ' has quitted.  Close this box to go back to the lobby.'
                    });
                }
            });
            //The server tells me that my opponent left the game by clicking on the back button
            socket.on('leave game', function(data) {
                $scope.gameEnded = true;
                addAlert($scope.alertsException, {
                    msg: 'Your opponent ' + $scope.userOpponent.nickname + ' has quitted.  Close this box to go back to the lobby.'
                });
            });
            socket.on('game move', function(data) {
                $scope.$apply(function() {
                    $scope.gameMoveFromOpponent = data;
                });
                $scope.gameMovesOpponent[data.row][data.column] = 1;
                if(checkForWin($scope.gameMovesOpponent) === true) {
                    addAlert($scope.alertsException, {
                        msg: 'You lose!  Close this box to go back to the lobby.'
                    });
                } else {
                    $scope.gameTurn = true;
                    addAlertWithTimer({
                        type: 'success',
                        msg: 'Your turn to go'
                    }, WAIT_TIME_SELF, true);
                }
            });
            $scope.closeAlertException = function(index) {
                var alert = $scope.alertsException[0];
                $scope.alertsException.splice(index, 1);
                $location.path('/');
            };
            $scope.userSelf = {
                socketId: appDataService.getUserSelfSocketId(),
                nickname: appDataService.getUserSelfNickname()
            };
            $scope.userOpponent = appDataService.getUserOpponent();
            $scope.alertsException = [];
            $scope.alertsTimer = [];
            $scope.winningCells = [];
            $scope.gameMovesSelf = new Array(SIZE);
            $scope.gameMovesOpponent = new Array(SIZE);
            (function(a, b) {
                for(var index = 0; index < SIZE; index++) {
                    a[index] = _.map(_.toArray(_.range(SIZE)), function(i) {
                        return 0;
                    });
                    b[index] = _.map(_.toArray(_.range(SIZE)), function(i) {
                        return 0;
                    });
                };
            })($scope.gameMovesSelf, $scope.gameMovesOpponent);
            $scope.gameEnded = false;
            $scope.gameTurn = appDataService.goFirst;
            $scope.gameChar = (appDataService.goFirst === true) ? 'X' : 'O';
            $scope.gameMoveToOpponent = {};
            $scope.gameMoveFromOpponent = {};
            $scope.$watch('gameMoveToOpponent', function() {
                if($scope.gameMoveToOpponent !== undefined && $scope.gameMoveToOpponent.row !== undefined && $scope.gameMoveToOpponent.column !== undefined) {
                    var row = $scope.gameMoveToOpponent.row;
                    var column = $scope.gameMoveToOpponent.column;
                    $scope.gameMovesSelf[row][column] = 1;
                    socket.emit('game move', {
                        'opponentSocketId': $scope.userOpponent.socketId,
                        'row': row,
                        'column': column
                    });
                    if(checkForWin($scope.gameMovesSelf) === true) {
                        addAlert($scope.alertsException, {
                            msg: 'You win!  Close this box to go back to the lobby.'
                        });
                    } else {
                        addAlertWithTimer({
                            type: 'danger',
                            msg: 'Wait for move from opponent'
                        }, WAIT_TIME_OPPONENT, false);
                    }
                }
            });
            if(appDataService.getUserOpponent().socketId === undefined) {
                addAlert($scope.alertsException, {
                    msg: 'You are not in a game against anyone.  Close this box to go back to the lobby.'
                });
            } else if(appDataService.goFirst === true) {
                addAlertWithTimer({
                    type: 'success',
                    msg: 'Your turn to go'
                }, WAIT_TIME_SELF, true);
            }
        }
    ]);
})(angular.module('caro'));