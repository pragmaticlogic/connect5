(function() {
    'use strict';
    angular.module('caro.appdata.service', []).
    factory('appDataService', ['$rootScope',
        function($rootScope) {
            var userSelf = {
                socketId: undefined,
                nickname: undefined
            },
                userOpponent = {
                    socketId: undefined,
                    nickname: undefined
                },
                roomId,
                goFirst;
            return {
                roomId: roomId,
                goFirst: goFirst,
                setUserSelfSocketId: function(socketId) {
                    userSelf.socketId = socketId;
                },
                getUserSelfSocketId: function() {
                    return userSelf.socketId;
                },
                setUserSelfNickname: function(nickname) {
                    userSelf.nickname = nickname;
                },
                getUserSelfNickname: function() {
                    return userSelf.nickname;
                },
                setUserOpponent: function(opponent) {
                    userOpponent = opponent;
                },
                getUserOpponent: function() {
                    return userOpponent;
                }
            };
        }
    ]);
})();