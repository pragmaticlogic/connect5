(function() {
    'use strict';
    angular.module('caro.timer.service', []).
    factory('timerService', ['$rootScope', '$timeout',
        function($rootScope, $timeout) {
            var subscribers = [];
            (function() {
                var timeInSeconds = 0,
                    increment = 1, // 1s
                    timerId = undefined,
                    updateProgress = function(timerId) {
                        $timeout.cancel(timerId);
                        timeInSeconds += increment;
                        var i;
                        for(i = 0; i < subscribers.length; i++) {
                            if(timeInSeconds % subscribers[i].interval == 0) {
                                var args = subscribers[i].args;
                                args.push(subscribers[i].callback); //add callback to argument list
                                subscribers[i].callback.apply(undefined, args);
                            }
                        }
                    },
                    scheduleNext = function() {
                        timerId = $timeout(function() {
                            updateProgress(timerId);
                            scheduleNext();
                        }, increment * 1000);
                    };
                scheduleNext();
            })();
            return {
                subscribe: function(callback, interval, args) {
                    subscribers.push({
                        callback: callback,
                        interval: interval,
                        args: args
                    });
                },
                unsubscribe: function(callback) {
                    var i;
                    for(i = 0; i < subscribers.length; i++) {
                        if(subscribers[i].callback === callback) subscribers.splice(i, 1);
                    }
                }
            };
        }
    ]);
})();