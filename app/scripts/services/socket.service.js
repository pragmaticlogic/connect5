(function() {
    'use strict';
    angular.module('caro.socket.service', []).
    factory('socket', ['$rootScope',
        function($rootScope) {
            if(!window.location.origin) {
                window.location.origin = window.location.protocol + '//' + window.location.host;
            }
            var socketIo = io.connect(window.location.origin, {
                port: window.location.port === '' ? '80' : window.location.port,
                transports: ['websocket']
            });
            (function() {})();
            return {
                on: function(event, callback) {
                    socketIo.on(event, callback);
                },
                emit: function(eventName, data, callback) {
                    socketIo.emit(eventName, data, function() {
                        var args = arguments;
                        if(callback) {
                            callback.apply(socketIo, args);
                        }
                    });
                },
                join: function(room) {
                    socketIo.join(room);
                }
            };
        }
    ]);
})();