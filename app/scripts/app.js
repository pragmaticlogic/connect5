(function() {
    'use strict';
    angular.module('caro', ['ngCookies', 'ngResource', 'ngSanitize', 'ngRoute', 'xeditable', 'caro.timer.service', 'caro.socket.service', 'caro.appdata.service', 'ui.bootstrap', 'ui.bootstrap.progresscircle', 'window.unload.directives', 'clickable.grid']).config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.when('/', {
                templateUrl: 'views/partials/main.html',
                controller: 'MainCtrl'
            }).when('/play', {
                templateUrl: 'views/partials/play.html',
                controller: 'GameCtrl'
            }).otherwise({
                redirectTo: '/'
            });
            $locationProvider.html5Mode(true);
        }
    ]).run(['$rootScope', '$route', '$location', 'editableOptions',
        function($rootScope, $route, $location, editableOptions) {
            editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
            window.onbeforeunload = function() {
                return 'Please consider NOT reloading/refreshing the page.';
            };
        }
    ]);
})();