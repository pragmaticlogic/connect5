'use strict';

var api = require('./controllers/api'),
    index = require('./controllers'),
    util = require('util'),
	io = require('socket.io'),
	PlayerProvider = require('./models/player').PlayerProvider(),
	ioSocket;

var index = function(req, res) {
  	res.render('index');
};

var changeNickname = function(req, res) {
	var clientId = req.body.clientId;
	var nickname = req.body.nickname;
	console.log('clientId = ' + clientId);
	console.log('nickname = ' + nickname);

	var player = PlayerProvider.findPlayerById(clientId);
	if (!player) {
		util.log('Player not found: ' + clientId);
		return;
	}

	PlayerProvider.changeNickname(player, nickname);

	var players = PlayerProvider.findAll();
	//http://stackoverflow.com/questions/10342681/whats-the-difference-between-io-sockets-emit-and-broadcast
	
	//this.emit('all players', {'players': players});
	//this.broadcast.emit('all players', {'players': players});
	ioSocket.sockets.emit('all players', {'players': players});

	res.json({'response': 'ok'});
};

var inviteToPlay = function(req, res) {
	var inviteFromClientId = req.body.inviteFrom;
	var inviteToClientId = req.body.inviteTo;
	var fromNickname = req.body.fromNickname;

	console.log(inviteFromClientId);
	console.log(inviteToClientId);
	
    //SO 24041220
	ioSocket.to(inviteToClientId).emit('invite to play', {'inviteFromClientId': inviteFromClientId,
		'fromNickname': fromNickname});

	res.json({'response': 'ok'});
};

var cancelInvite = function(req, res) {	
	var inviteFromClientId = req.body.inviteFrom;
	var inviteToClientId = req.body.inviteTo;
	var fromNickname = req.body.fromNickname;

	ioSocket.to(inviteToClientId).emit('cancel invite', {'inviteFromClientId': inviteFromClientId,
		'fromNickname': fromNickname});

	res.json({'response': 'ok'});
};

var rejectInvite = function(req, res) {	
	var rejectToClientId = req.body.rejectTo;

	ioSocket.to(rejectToClientId).emit('reject invite', {'rejectToClientId': rejectToClientId});

	res.json({'response': 'ok'});
};

var acceptInvite = function(req, res) {	
	var acceptFromClientId = req.body.acceptFrom,
		roomId = req.body.roomId;

	ioSocket.to(acceptFromClientId).emit('accept invite', {
		'acceptFromClientId': acceptFromClientId,
		'roomId': roomId
	});

	res.json({'response': 'ok'});
};

var syncMyData = function(req, res) {
	var myClientId = req.body.myClientId,
		player = PlayerProvider.findPlayerById(myClientId);

	ioSocket.to(myClientId).emit('my socketId', {'socketId': myClientId, 'nickname': player.nickname, 'now': Date.now()}); //send the clientId back to the socket who just connects

	res.json({'response': 'ok'});
};

var broadcastAllPlayers = function(req, res) {	
	var players = PlayerProvider.findAll();
	ioSocket.sockets.emit('all players', {'players': players});
	
	res.json({'response': 'ok'});
};

var checkNicknameUnique = function(req, res) {
	var clientId = req.body.clientId;
	var nickname = req.body.nickname;
	console.log('clientId = ' + clientId);
	console.log('nickname = ' + nickname);

	var player = PlayerProvider.findPlayerByNickname(nickname);
	if (!player) {
		res.json({'response': 'ok'});
	} else {
		res.json({'response': 'not-ok'});
	}	
};

var onConnect = function(client) {	
	
	var players = PlayerProvider.findAll();
	util.log('players = ' + players);	

	//google.com/search?q=socket.io+send+to+specific+user&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a
	client.emit('my socketId', {'socketId': client.id, 'now': Date.now()}); //send the clientId back to the socket who just connects

	client.on('disconnect', function() {		
		util.log('Player has disconnected ' + this.id);

		this.broadcast.emit('remove player', {id: this.id});
	
		var removePlayer = PlayerProvider.findPlayerById(this.id);
		if (!removePlayer) {
			util.log('Player not found: ' + this.id);
			return;
		}

		PlayerProvider.removePlayer(removePlayer);
	
		var players = PlayerProvider.findAll();
		//http://stackoverflow.com/questions/10342681/whats-the-difference-between-io-sockets-emit-and-broadcast
		
		//this.emit('all players', {'players': players});
		//this.broadcast.emit('all players', {'players': players});
		ioSocket.sockets.emit('all players', {'players': players});
	});

	client.on('new player', function(data) {		
		this.broadcast.emit('new player', {id: this.id, nickname: data.nickname, logonTime: data.logonTime});

		PlayerProvider.addPlayer(this.id, data.nickname);

		var players = PlayerProvider.findAll();
		
		//this.emit('all players', {'players': players});
		//this.broadcast.emit('all players', {'players': players});
		ioSocket.sockets.emit('all players', {'players': players});
	});

	client.on('leave game', function(data) {
		ioSocket.to(data.opponentSocketId).emit('leave game', {});
	});

	client.on('game move', function(data) {
		ioSocket.to(data.opponentSocketId).emit('game move', data);
	});
};

/**
 * Application routes
 */
module.exports = function(app, server) {	
	ioSocket = io.listen(server, function() {
  		console.log("IO Socket listens on port " + server);
	});
	
	ioSocket.set('log level', 1);
	ioSocket.sockets.on('connection', onConnect);	

	// Server API Routes
	//app.get('/api/awesomeThings', api.awesomeThings);


	// All other routes to use Angular routing in app/scripts/app.js
	//app.get('/partials/*', index.partials);
	//app.get('/*', index.index);
	
	app.get('/', index);
	app.post('/check-nickname-unique', checkNicknameUnique);
	app.post('/change-nickname', changeNickname);
	app.post('/invite-to-play', inviteToPlay);
	app.post('/cancel-invite', cancelInvite);
	app.post('/reject-invite', rejectInvite);
	app.post('/accept-invite', acceptInvite);	
	app.post('/sync-my-data', syncMyData);
	app.post('/broadcast-all-players', broadcastAllPlayers);
};