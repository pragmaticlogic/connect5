var Player = function(id, nickname) {
	this.id = id;
	this.nickname = nickname;
	this.logonTime = Date.now();
};

module.exports.PlayerProvider = function() {
	var players = [];

	return {		
		findAll: function() {
			return players;
		},

		findPlayerById: function(id) {
			var i;
			for (i = 0; i < players.length; i++) {
				if (players[i].id == id) 
					return players[i];
			}

			return false;
		},

		findPlayerByNickname: function(nickname) {
			var i;
			for (i = 0; i < players.length; i++) {
				if (players[i].nickname == nickname) 
					return players[i];
			}

			return false;
		},

		addPlayer: function(id, nickname) {
			var player = new Player(id, nickname);			
			players.push(player);
			console.log(players.length + ' players');
		},

		removePlayer: function(removePlayer) {
			players.splice(players.indexOf(removePlayer), 1);
		},

		changeNickname: function(player, nickname) {
			player.nickname = nickname;	
		}
	};
};

//exports.PlayerProvider = PlayerProvider;