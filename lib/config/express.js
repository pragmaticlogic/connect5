'use strict';
var express = require('express'),
    path = require('path'),
    config = require('./config');
/**
 * Express configuration
 */
module.exports = function(app) {
    app.configure('development', function() {
                
        // Disable caching of scripts for easier testing
        app.use(function noCache(req, res, next) {
            if(req.url.indexOf('/scripts/') === 0) {
                res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
                res.header('Pragma', 'no-cache');
                res.header('Expires', 0);
            }
            next();
        });
        
        console.log('ENV ==> development');
        app.use(express.favicon(path.join(config.root, '/app/dist', 'favicon.ico')));
        app.use(express.static(path.join(config.root, '/app')));
        app.use(express.errorHandler());
        app.set('views', config.root + '/app/views');
    });
    app.configure('production', function() {
        console.log('ENV ==> production');
        app.use(express.favicon(path.join(config.root, '/app/dist', 'favicon.ico')));
        app.use(express.static(path.join(config.root, '/app/dist')));
        app.set('views', config.root + '/app/dist');
    });
    app.configure(function() {
        console.log('ENV ==> all');
        
        app.engine('html', require('ejs').renderFile);
        app.set('view engine', 'html');
        app.use(express.logger('dev'));
        app.use(express.json());
        app.use(express.urlencoded());
        app.use(express.methodOverride());
        // Router needs to be last
        app.use(app.router);
    });
};