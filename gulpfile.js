// Include gulp
var gulp = require('gulp');
var del = require('del');
var jshint = require('gulp-jshint');
var minifycss = require('gulp-minify-css');
var concat = require('gulp-concat');
var ngannotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var server = require('gulp-develop-server');
var useref = require('gulp-useref'),
    assets = useref.assets();
var gulpif = require('gulp-if');
// Clean dist
// Delete the dist directory
gulp.task('clean', function(cb) {
    return del(['app/dist/**/*.*'], cb);
});
//Copy partials html
gulp.task('copypartials', ['clean'], function() {
    return gulp.src(['app/views/partials/*.*']).pipe(gulp.dest('app/dist/views/partials'));
});
// Lint Task
gulp.task('lint', function() {
    return gulp.src('app/scripts/*.js').pipe(jshint()).pipe(jshint.reporter('default'));
});
// Minify css
gulp.task('minifycss', function() {
    gulp.src('app/styles/*.css').pipe(concat('combined.css')).pipe(minifycss()).pipe(gulp.dest('app/dist'));
});
// Concatenate & annotate & minify app js
gulp.task('scripts', function() {
    return gulp.src(['app/scripts/*.js', 'app/scripts/controllers/*.js', 'app/scripts/directives/*.js', 'app/scripts/services/*.js']).pipe(concat('app.js')).pipe(gulp.dest('app/dist')).pipe(ngannotate()).pipe(rename('app.min.js')).pipe(uglify()).pipe(gulp.dest('app/dist'));
});
// Concatenate & annotate & minify thirdparty js
gulp.task('scriptsthird', function() {
    return gulp.src(['app/bower_components/jquery/dist/jquery.js', 'app/bower_components/angular/angular.js', 'app/bower_components/bootstrap/dist/js/bootstrap.js', 'app/bower_components/angular-resource/angular-resource.js', 'app/bower_components/angular-cookies/angular-cookies.js', 'app/bower_components/angular-sanitize/angular-sanitize.js', 'app/bower_components/angular-route/angular-route.js', 'app/bower_components/moment/moment.js', 'app/bower_components/lodash/dist/lodash.compat.js']).pipe(concat('third.js')).pipe(ngannotate()).pipe(rename('third.min.js')).pipe(uglify()).pipe(gulp.dest('app/dist'));
});
// Replace references to non-optimized scripts or stylesheets
gulp.task('html', ['copypartials'], function() {
    return gulp.src('app/views/*.html').pipe(assets).pipe(gulpif('*.js', uglify())).pipe(gulpif('*.css', minifycss())).pipe(assets.restore()).pipe(useref()).pipe(gulp.dest('app/dist'));
});
// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('app/scripts/*.js', ['lint', 'scripts']);
    gulp.watch('app/styles/*.css', ['minifycss']);
});
// Build Task
gulp.task('build', ['clean', 'copypartials', 'lint', 'minifycss', 'scripts', 'scriptsthird', 'html']);
// Server task
gulp.task('server:start', function() {
    server.listen({
        path: './server.js'
    });
});
// restart server if app.js changed
gulp.task('server:restart', function() {
    gulp.watch('app/scripts/*.js', ['lint', 'scripts'], server.restart);
    gulp.watch('app/styles/*.css', ['minifycss'], server.restart);
});
//Default
gulp.task('default', ['server:start']);